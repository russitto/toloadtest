const path = require('path')
const baseDir = path.join(__dirname, '..', '..')

module.exports = {
  apps: [{
    name: 'toLoadTest',
    script: './server.js',
    instance_var: 'INSTANCE_ID',
    instances: 'max',
    exec_mode: 'cluster',
    env: {
      PORT: 3000
    },
  }]
}
