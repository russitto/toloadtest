#!/user/bin/env node

const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
  let name = 'Mundo'
  if (req.query && req.query.name) {
    name = req.query.name
  }
  res.send(`Hola ${name}!`)
})

app.get('/wait', (req, res) => {
  const milli = 200
  setTimeout(() => {
    res.send('I waited ms: ' + milli)
  }, milli)
})

const toOrder = [
  '830787c955231ffe26d2dfaec8d78187',
  '7a9981eb3033a7df1f373b1ec28c543e',
  '931d275d94c4b58176d861f9efab8dc9',
  'a87d796baeb734ac0fd9c0bc82741e15',
  '1c89571927b060a66d77a71173a074e3',
  '3fb66de3c49b46a937497acac276a79b',
  '338976e1ab63eb2548ec2fc124119626',
  'd335fdeafa61f5af41250b232bdb6581',
  '7123016c2d0cc826fb3f3eec281cdf7d',
  '1b99a37b6dec38bbde164959b44eaace',
  '3b04fa121cb372a6963ed7dcbdba6172',
  '2bdf5c0f9c8e1d339bc2fb10585a0d45',
  '5962a9bf6b68f8cbdd2db49178b84492',
  'cd85931c736dfb656afad9aafaa2c6d0',
  'e4860adc30344f6bd7de39cc526226b1',
  '58c724f627d2a726c5ba64fd1df45a0b',
  '6d5b4f36daa97acc3d896aa0617abad7',
  '712f2a75eae5e8a900a6a66d56d9a3d4',
  'e04204689a8ab62da0ddf039f1c1850e',
  '77ceaa4c672e6f353562e568c23b0397',
]

app.get('/order', (req, res) => {
  for (let i = 0; i < 1000000; i++) {
    sortArr()
  }
  res.send('cloned & sorted 1000000 times!')
})

function sortArr() {
  const arr = JSON.parse(JSON.stringify(toOrder))
  for (let i = 0; i < toOrder.length; i++) {
    for (let j = i + 1; j < toOrder.length; j++) {
      if (arr[i] > arr[j]) {
        let swap = arr[i]
        arr[i] = arr[j]
        arr[j] = swap
      }
    }
  }
}

app.listen(port, () => console.log('Listening on:', port))
